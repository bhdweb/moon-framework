# moon-framework

### 目的
在基于Spring boot/cloud的微服务项目中，要创建若干module，每个module或多或少都集成一些第三方依赖（如：Redis、mybatis）和一些基础工具类等等， 而这些工作往往都是重复劳动，对于初学者来说还有一定的上手门槛。
因此本项目通过一定的规则来统一编写各种依赖的starter，方便在企业内部进行规则的统一，配置的统一，用法的统一，监控的统一，部署的统一，同时分离通用框架代码和业务代码的耦合，提炼公共服务配置和工具，进一步的简化Spring Boot和Spring Cloud的使用。

## 特别说明
对于同类型的框架，github和gitee有太多优秀的项目了，但我为什么还要重复造一个呢？原因如下：

- 加深自已对这方面技术的理解
- 从业这么多年，一直也没有系统性的留下什么东西，moon这一系列的项目算是对自已从业这么多年的一个总结，为以后留个念想
- 市场上所见过的这类型框架的starter，都是将代码分散地写在各自已的starter里，个人觉得这种作法对于纯开源项目是可取的，但对于企业如果想基于这个进行二次开发就不太友好了。代码过于分散，阅读、调试时需要切换多个module，维护成本高。本项目的做法是将所有java代码都按包结构隔离封装在`moon-framework-autoconfigure`里，各自starter只做配置`spring.factories`

> 上面第三点纯属个人观点，不喜欢可以忽略

## 项目名称moon由来
moon(广寒宫),为什么取这个名字呢？因为我家的嫦娥妹妹啊！！！

## 愿景
希望本项目越来越强大，基于更多的自动化配置和抽象，能在项目开发中节约更多的时间。

## 项目结构
```
├── moon-framework                                     项目父级目录
├── moon-framework-autoconfigure                    功能实现，所有的代码都写在这里
├── moon-framework-dependencies                     依赖版本统一管理
├── moon-framework-starters                         核心starter模块工程
	├── moon-framework-starter-boot                     核心库(安全验证、缓存、跨域、统一日志、请求统一返回体、自定义业务异常、Jackson序列化/反序列化配置、常用工具类等)
	├── moon-framework-starter-mybatis-plus             基于mybatis-plus集成orm框架(数据权限过滤、基于行级(column)租户SaaS方式实现)
	├── moon-framework-starter-dynamic-datasource       基于`AbstractRoutingDataSource`的动态数据源实现
	├── moon-framework-starter-tenant                   基于数据源(datasource)租户Saas方式实现
	├── moon-framework-starter-oss                      对象存储(本地文件、minio、七牛云)
	├── moon-framework-starter-redis                    redis的封装(分布式缓存、分布锁、消息订阅发布mq)
	├── moon-framework-starter-rabbitmq                 基于rabbitmq的mqtt
	├── moon-framework-starter-cloud                    Spring cloud脚手架（基于Spring Cloud Alibaba）

```    


## 快速开始

### 第一步，下载本项目

[本项目地址](https://gitee.com/bhdweb/moon-framework)

`git clone https://gitee.com/bhdweb/moon-framework.git`

### 第二步，编译安装本项目

   `mvn clean install` --项目会编译安装到本地maven仓库

### 第三步，在自己的工程按需添加依赖库

1. 在项目parent pom.xml中添加：
```
<properties>
	<moon.version>1.0.0-GA</moon.version>
	<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	<maven.compiler.source>1.8</maven.compiler.source>
	<maven.compiler.target>1.8</maven.compiler.target>
</properties>
```
2. 添加需要的starter(如moon-framework-start-boot)
```
<dependency>
	<groupId>org.moon.framework</groupId>
	<artifactId>moon-framework-starter-boot</artifactId>
	<version>${moon.version}</version>
</dependency>
```
## Demo
具体代码可以参考
[moon-framework-samples具体的demo](https://gitee.com/bhdweb/moon-framework-samples)

## 基于moon-framework的最佳实践

> 基于moon-framework框架，提供一个springboot的单体实现和一个springcloud Alibaba的微服务实现。这两个实现共用一套前端代码moon-vue
### 包含功能


### springboot单体框架

具体代码可以查看
[moon-boot单体架构](https://gitee.com/bhdweb/moon-boot)

### springCloud Alibaba 微服务框架

具体代码可以查看
[moon-cloud微服务架构](https://gitee.com/bhdweb/moon-cloud)

### moon-vue 前端代码
具体代码可以查看
[moon-vue](https://gitee.com/bhdweb/moon-vue)

