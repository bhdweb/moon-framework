/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.moon.framework.autoconfigure.springmvc.response.ErrorCode;

import java.util.List;

/**
 * 系统常量
 * @author moon
 */
public interface MoonConstants {

	ErrorCode SUCCESS = new ErrorCode("S200", "操作成功");
	ErrorCode FAIL = new ErrorCode("S500", "操作失败");

	ErrorCode ACCESS_DENIED = new ErrorCode("S601", "您的访问未授权，请联系管理员");
	ErrorCode AUTHENTICATION = new ErrorCode("S602", "身份验证失败,请输入正确信息");


	ErrorCode METHOD_ARGUMENT = new ErrorCode("S501", "方法参数验证错误");
	ErrorCode MISSING_ARGUMENT = new ErrorCode("S502", "方法参数缺失");
	ErrorCode FEIGN = new ErrorCode("S503","服务熔断降级");
	ErrorCode LIMIT = new ErrorCode("S504", "系统限流");
	ErrorCode IDEMPOTENT = new ErrorCode("S505", "请不要重复操作");


	ErrorCode OSS_CONFIG_NOT_EXISTS = new ErrorCode("S510", "请配置对象存储");
	ErrorCode OSS_UPLOAD_ERROR = new ErrorCode("S511", "文件操作失败");
	ErrorCode OSS_CONFIG_NOT_SUPPORT = new ErrorCode("S512", "不支持的对象存储类型");
	ErrorCode SMS_CONFIG_NOT_EXISTS = new ErrorCode("S520", "请配置短信运营商");

	/**
	 * 静态资源路径
	 */
	List<String> STATIC_RESOUCE = Lists.newArrayList(".png",".jpeg",".ico",".jpg",".css",".html",".js",
			"/actuator/health/**","/v2/api-docs-ext/**","/swagger-ui.html","/doc.html","/favicon.ico","/csrf","/swagger**","/swagger-resources/**","/webjars/**");

	/**
	 * 微服务之间传递的唯一标识
	 */
	String HEADER_REQUEST_ID = "request-id";

	/**
	 * 日志链路追踪id日志标志
	 */
	String LOG_TRACE_ID = "traceId";

	/**
	 * 错误日志
	 */
	String REDIS_KEY_ERROR_LOG = "log:error";
	/**
	 * 操作日志
	 */
	String REDIS_KEY_OPERATE_LOG = "log:operate";

	/**
	 * 删除本地缓存
	 */
	String REDIS_KEY_DEL_LOCAL = "del_local_cache";

	@Getter
	@AllArgsConstructor
	enum RoleTypeEnum {

		/**
		 * 内置角色
		 */
		SYSTEM(1),
		/**
		 * 自定义角色
		 */
		CUSTOM(2);

		private final Integer type;

	}
	/**
	 * 数据范围枚举类
	 * 用于实现数据级别的权限
	 */
	@Getter
	@AllArgsConstructor
	enum DataScopeEnum {

		ALL(1), // 全部数据权限

		DEPT_CUSTOM(2), // 指定部门数据权限
		DEPT_ONLY(3), // 部门数据权限
		DEPT_AND_CHILD(4), // 部门及以下数据权限

		SELF(5); // 仅本人数据权限

		/**
		 * 范围
		 */
		private final Integer scope;

	}
	/**
	 * 数据状态
	 * (0-启用,1-禁用)
	 * @author moon
	 */
	enum DataStatusEnum {
		/**
		 * 0-启用
		 */
		enabled,
		/**
		 * 1-禁用
		 */
		disabled
	}

	/**
	 * 租户类型
	 */
	enum TenantTypeEnum{
		/**
		 * 不开启租户模式
		 */
		none,
		/**
		 * 列级租户模式
		 */
		column,
		/**
		 * 数据源级别租户模式
		 */
		datasource
	}

	/**
	 * sms存储方式
	 */
	enum SmsTypeEnum{
		aliyun,qiniu,tencent
	}

	/**
	 * 当前登录用户的信息
	 */
	String CACHE_AUTH_INFO = "auth:";

	/**
	 * 登录过期时间
	 */
	long SESSION_TIME_OUT = 60*60*1000L;

	/**
	 * 刷新登录信息
	 */
	long REFRESH_SESSION = 10*60*1000L;

	/**
	 * 功能权限
	 */
	String CACHE_PERMISSION = "permission:";


	/**
	 * header token访问标识
	 */
	String HEADER_ACCESS_TOKEN = "access-token";

	/**
	 * JWT存储权限属性(token解码之后的payload)
	 */
	String JWT_AUTHORITIES_KEY = "authorities";
	/**
	 * header 请求来源标识
	 */
	String REQUEST_FROM = "rfrom";
	/**
	 * header 租户编号
	 */
	String HEADER_TENANT_CODE = "tenant-code";
	
	/**
	 * 默认租户
	 */
	String DEFAULT_TENANT_CODE = "10000";

	/**
	 * 管理员角色编码
	 */
	String ADMIN_CODE = "GLY";

	/**
	 * 管理员用户名
	 */
	String DEFAULT_USER_NAME = "moonadmin";
	
	/**
	 * 请求来源类型
	 * @author moon
	 */
	public enum RequestFormEnum {
		feign,platform,yapp
	}
}