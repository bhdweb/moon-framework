package org.moon.framework.autoconfigure.oss.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import lombok.SneakyThrows;
import org.moon.framework.autoconfigure.exception.domain.MoonException;

import java.io.File;

/**
 * 文件工具类
 *
 * @author 芋道源码
 */
public class FileUtils extends FileUtil{

    /**
     * 创建临时文件
     * 该文件会在 JVM 退出时，进行删除
     *
     * @param data 文件内容
     * @return 文件
     */
    @SneakyThrows
    public static File createTempFile(String data) {
        File file = createTempFile();
        // 写入内容
        writeUtf8String(data, file);
        return file;
    }

    /**
     * 创建临时文件
     * 该文件会在 JVM 退出时，进行删除
     *
     * @param data 文件内容
     * @return 文件
     */
    @SneakyThrows
    public static File createTempFile(byte[] data) {
        File file = createTempFile();
        // 写入内容
        writeBytes(data, file);
        return file;
    }

    /**
     * 创建临时文件，无内容
     * 该文件会在 JVM 退出时，进行删除
     *
     * @return 文件
     */
    @SneakyThrows
    public static File createTempFile() {
        // 创建文件，通过 UUID 保证唯一
        File file = File.createTempFile(IdUtil.simpleUUID(), null);
        // 标记 JVM 退出时，自动删除
        file.deleteOnExit();
        return file;
    }

    /**
     * 生成文件路径
     * @param originalName 原始文件名
     * @return path，唯一不可重复
     */
    public static String generatePath(String originalName) {
        if (StrUtil.isBlank(originalName)) {
            throw new MoonException("请传入文件名");
        }
        String path = System.currentTimeMillis()+"/"+originalName;
        return path;
    }
}
