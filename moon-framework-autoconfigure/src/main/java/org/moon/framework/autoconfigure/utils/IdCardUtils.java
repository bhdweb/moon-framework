/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 身份证工具类
 * @author moon
 */
public class IdCardUtils {

	/**
	 * 验证身份证合法性
	 */
	public static String validateIDCard(String IDStr) {
		String errorInfo = "";// 记录错误信息
		String[] ValCodeArr = { "1", "0", "x", "9", "8", "7", "6", "5", "4",
				"3", "2" };
		String[] Wi = { "7", "9", "10", "5", "8", "4", "2", "1", "6", "3", "7",
				"9", "10", "5", "8", "4", "2" };
		String Ai = "";
		// ================ 号码的长度 15位或18位 ================
		if (IDStr.length() != 15 && IDStr.length() != 18) {
			errorInfo = "身份证号码长度应该为15位或18位。";
			return errorInfo;
		}
		// =======================(end)========================

		// ================ 数字 除最后以为都为数字 ================
		if (IDStr.length() == 18) {
			Ai = IDStr.substring(0, 17);
		} else if (IDStr.length() == 15) {
			Ai = IDStr.substring(0, 6) + "19" + IDStr.substring(6, 15);
		}
		if (isNumeric(Ai) == false) {
			errorInfo = "身份证15位号码都应为数字 ; 18位号码除最后一位外，都应为数字。";
			return errorInfo;
		}
		// =======================(end)========================

		// ================ 出生年月是否有效 ================
		String strYear = Ai.substring(6, 10);// 年份
		String strMonth = Ai.substring(10, 12);// 月份
		String strDay = Ai.substring(12, 14);// 月份
		if (isDate(strYear + "-" + strMonth + "-" + strDay) == false) {
			errorInfo = "身份证生日无效。";
			return errorInfo;
		}
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if ((gc.get(Calendar.YEAR) - Integer.parseInt(strYear)) > 150
					|| (gc.getTime().getTime() - s.parse(
							strYear + "-" + strMonth + "-" + strDay).getTime()) < 0) {
				errorInfo = "身份证生日不在有效范围。";
				return errorInfo;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		if (Integer.parseInt(strMonth) > 12 || Integer.parseInt(strMonth) == 0) {
			errorInfo = "身份证月份无效";
			return errorInfo;
		}
		if (Integer.parseInt(strDay) > 31 || Integer.parseInt(strDay) == 0) {
			errorInfo = "身份证日期无效";
			return errorInfo;
		}
		// =====================(end)=====================

		// ================ 地区码时候有效 ================
		Map<Integer, String> h = GetAreaCode();
		if (h.get(Integer.parseInt(Ai.substring(0, 2))) == null) {
			errorInfo = "身份证地区编码错误。";
			return errorInfo;
		}
		// ==============================================

		// ================ 判断最后一位的值 ================
		int TotalmulAiWi = 0;
		for (int i = 0; i < 17; i++) {
			TotalmulAiWi = TotalmulAiWi
					+ Integer.parseInt(String.valueOf(Ai.charAt(i)))
					* Integer.parseInt(Wi[i]);
		}
		int modValue = TotalmulAiWi % 11;
		String strVerifyCode = ValCodeArr[modValue].toUpperCase();
		Ai = Ai + strVerifyCode;

		if (IDStr.length() == 18) {
			if (Ai.equals(IDStr) == false) {
				errorInfo = "身份证无效，不是合法的身份证号码";
				return errorInfo;
			}

		} else {
			return "";
		}
		// =====================(end)=====================
		return "";
	}

	/**
	 * 功能：设置地区编码
	 */
	private static Map<Integer, String> GetAreaCode() {
		Map<Integer, String> hashtable = new HashMap<Integer, String>();
		hashtable.put(11, "北京");
		hashtable.put(12, "天津");
		hashtable.put(13, "河北");
		hashtable.put(14, "山西");
		hashtable.put(15, "内蒙古");
		hashtable.put(21, "辽宁");
		hashtable.put(22, "吉林");
		hashtable.put(23, "黑龙江");
		hashtable.put(31, "上海");
		hashtable.put(32, "江苏");
		hashtable.put(33, "浙江");
		hashtable.put(34, "安徽");
		hashtable.put(35, "福建");
		hashtable.put(36, "江西");
		hashtable.put(37, "山东");
		hashtable.put(41, "河南");
		hashtable.put(42, "湖北");
		hashtable.put(43, "");
		hashtable.put(44, "广东");
		hashtable.put(45, "广西");
		hashtable.put(46, "海南");
		hashtable.put(50, "重庆");
		hashtable.put(51, "四川");
		hashtable.put(52, "贵州");
		hashtable.put(53, "云南");
		hashtable.put(54, "西藏");
		hashtable.put(61, "陕西");
		hashtable.put(62, "甘肃");
		hashtable.put(63, "青海");
		hashtable.put(64, "宁夏");
		hashtable.put(65, "新疆");
		hashtable.put(71, "台湾");
		hashtable.put(81, "香港");
		hashtable.put(82, "澳门");
		hashtable.put(91, "国外");
		return hashtable;
	}

	/**
	 * 功能：判断字符串是否为数字
	 * 
	 * @param str
	 * @return
	 */
	private static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (isNum.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 功能：判断字符串是否为日期格式
	 * 
	 * @param str
	 * @return
	 */
	private static boolean isDate(String strDate) {
		Pattern pattern = Pattern
				.compile("^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$");
		Matcher m = pattern.matcher(strDate);
		if (m.matches()) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 根据身份证号码输出省份
	 */
	public static String idNoToProvince(String idNo){
		String errMsg = validateIDCard(idNo);
    	if(Func.isNotEmpty(errMsg)){
    		return "";
    	}
		Map<Integer, String> map = GetAreaCode();
		String str = idNo.substring(0, 2);
		return map.get(Integer.valueOf(str));
	}
	
	public static void main(String[] args) {
		System.err.println(idNoToProvince("430421198710052973"));
	}
	
	/**
	 * 根据身份证号输出年龄
	 */
    public static int idNoToAge(String idNo){
    	String errMsg = validateIDCard(idNo);
    	if(Func.isNotEmpty(errMsg)){
    		return 0;
    	}
        int leh = idNo.length();
        String dates="";
        if (leh == 18) {
            dates = idNo.substring(6, 10);
            SimpleDateFormat df = new SimpleDateFormat("yyyy");
            String year=df.format(new Date());
            int u=Integer.parseInt(year)-Integer.parseInt(dates);
            return u;
        }else{
            dates = idNo.substring(6, 8);
            return Integer.parseInt(dates);
        }
    }
    
    /**
     * 根据身份证号输出性别 1=男
     */
    public static int idNoToSex(String idNo){
    	String errMsg = validateIDCard(idNo);
    	if(Func.isNotEmpty(errMsg)){
    		return -1;
    	}
    	 int idxSexStart = 16;  
         //如果是15位的证件号码  
         if(idNo.length() == 15) {  
             idxSexStart = 14;  
         }  
    	String idxSexStr = idNo.substring(idxSexStart, idxSexStart + 1);  
        int idxSex = Integer.parseInt(idxSexStr) % 2;  
        return idxSex;
    }
}
