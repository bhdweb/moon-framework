/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.log.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 访问日志配置 日志
 * @author ninglong
 */
@Configuration
@Data
@RefreshScope
@ConfigurationProperties(prefix = "moon.log.access-log")
public class AccessLogProperties {

  private boolean enabled = true;

  private boolean includeRequest = true;
  private boolean includeResponse = true;

  private int requestBodyLength = 8192;
  private int responseBodyLength = 8192;
}
