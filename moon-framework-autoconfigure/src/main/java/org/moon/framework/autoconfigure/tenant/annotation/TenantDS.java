package org.moon.framework.autoconfigure.tenant.annotation;

import java.lang.annotation.*;

/**
 * 需要切换数据源
 * @author moon
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TenantDS {
}
