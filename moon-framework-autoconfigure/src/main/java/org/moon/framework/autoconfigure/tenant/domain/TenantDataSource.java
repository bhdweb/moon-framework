/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.tenant.domain;

import lombok.Data;
import org.moon.framework.autoconfigure.datasource.domain.MoonDataSource;

/**
 * 租户数据源
 * @author moon
 */
@Data
public class TenantDataSource extends MoonDataSource {

    /**
     * 数据源ID
     */
    private String datasourceId;

    /**
     * 租户code
     */
    private String tenantCode;

    /**
     * 应用名称
     */
    private String applicationName;
}
