/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.springmvc.domain;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 分页结果对象
 * @author moon
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class MoonPage<T> implements Serializable{

	private static final long serialVersionUID = -8703818385018536959L;

	/**
     * 查询数据列表
     */
    private List<T> records = Collections.emptyList();

    /**
     * 总数
     */
    private long total = 0;
    /**
     * 每页显示条数，默认 10
     */
    private long size = 10;

    /**
     * 当前页
     */
    private long current = 1;
    
    public MoonPage(List<T> records,long total,long size,long current) {
    	this.records = records;
    	this.total = total;
    	this.size = size;
    	this.current = current;
    }
    
    public static <T> MoonPage<T> of(IPage<T> page) {
    	MoonPage<T> moon = new MoonPage<T>();
    	moon.setCurrent(page.getCurrent());
    	moon.setRecords(page.getRecords());
    	moon.setSize(page.getSize());
    	moon.setTotal(page.getTotal());
    	return moon;
    }
	public static <T> MoonPage<T> empty(Query query) {
		return build(Lists.newArrayList(),0,query);
	}

	public static <T> MoonPage<T> build(List<T> records,long total,Query query) {
		return new MoonPage(records,total,query.getPageSize(),query.getPageNo());
	}
}