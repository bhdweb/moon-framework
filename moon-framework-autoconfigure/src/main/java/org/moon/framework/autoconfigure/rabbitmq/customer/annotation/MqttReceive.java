package org.moon.framework.autoconfigure.rabbitmq.customer.annotation;

import java.lang.annotation.*;

/**
 * 消息接收指定topic
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface MqttReceive {
    String topicName() default "";
}
