/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.oss;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ReflectUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.moon.framework.autoconfigure.MoonConstants;
import org.moon.framework.autoconfigure.cache.CacheService;
import org.moon.framework.autoconfigure.config.SpringContextConfig;
import org.moon.framework.autoconfigure.exception.domain.MoonException;
import org.moon.framework.autoconfigure.oss.impl.*;
import org.moon.framework.autoconfigure.oss.props.OssProperties;
import org.moon.framework.autoconfigure.secure.AuthUtils;
import org.springframework.context.annotation.Configuration;

/**
 * 对象存储调用入口
 * @author 村口的大爷
 */
@Configuration
@Slf4j
public class OssTemplate {

    /**
     * 数据存储器对应的数据表名
     */
    public final static String DB_OSS_TABLE_NAME = "sys_oss_file";
    /**
     * 存储器配置缓存前缀
     */
    public final static String CACHE_OSS_CONFIG = "oss:config:";

    /**
     * 获取域名
     */
    public static String getDomain(String tenantCode){
        CacheService cacheService = SpringContextConfig.getBean(CacheService.class);
        OssProperties ossProperties = cacheService.get(CACHE_OSS_CONFIG+ tenantCode, OssProperties.class);
        if(ossProperties==null){
            throw new MoonException(MoonConstants.OSS_CONFIG_NOT_EXISTS);
        }
        return ossProperties.getDomain();
    }

    /**
     * 获取对象存储操作对象(默认当前租户)
     */
    public static OssService getOssTemplate(){
        return getOssTemplate(AuthUtils.getTenantCode());
    }

    /**
     * 获取对象存储操作对象
     */
    public static OssService getOssTemplate(String tenantCode){
        CacheService cacheService = SpringContextConfig.getBean(CacheService.class);
        OssProperties ossProperties = cacheService.get(CACHE_OSS_CONFIG+ tenantCode, OssProperties.class);
        if(ossProperties==null){
            throw new MoonException(MoonConstants.OSS_CONFIG_NOT_EXISTS);
        }
        FileStorageEnum storageEnum = FileStorageEnum.getByStorage(ossProperties.getStorage());
        if(storageEnum==null){
            log.error(String.format("文件配置(%s) 为空", storageEnum));
            throw new MoonException(MoonConstants.OSS_CONFIG_NOT_SUPPORT);
        }
        // 创建客户端
        return ReflectUtil.newInstance(storageEnum.getClientClass(), ossProperties);
    }

    /**
     * 文件存储器枚举
     * @author 村口的大爷
     */
    @AllArgsConstructor
    @Getter
    public enum FileStorageEnum {

        DB(0, DBOssService.class),

        LOCAL(1, LocalOssService.class),
        FTP(2,  FtpOssService.class),
        SFTP(3, SftpOssService.class),

        S3(4, S3OssService.class),
        ;

        /**
         * 存储器
         */
        private final Integer storage;

        /**
         * 客户端类
         */
        private final Class<? extends OssService> clientClass;

        public static FileStorageEnum getByStorage(Integer storage) {
            return ArrayUtil.firstMatch(o -> o.getStorage().equals(storage), FileStorageEnum.values());
        }
    }
}
