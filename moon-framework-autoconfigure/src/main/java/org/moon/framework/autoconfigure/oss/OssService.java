/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.oss;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.moon.framework.autoconfigure.config.SpringContextConfig;
import org.moon.framework.autoconfigure.oss.utils.FileUtils;
import org.moon.framework.autoconfigure.secure.AuthUtils;
import org.moon.framework.autoconfigure.secure.domain.AuthInfo;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Date;
import java.util.List;

/**
 * 对象存储服务
 * @author 村口的大爷
 */
public interface OssService {

    /**
     * 上传文件
     */
    String upload(byte[] content, String originalName);

    /**
     * 删除文件
     */
    void delete(String link);

    /**
     * 批量删除文件
     */
    void batchDelete(List<String> links);

    /**
     * 获取文件内容
     */
    byte[] getContent(String link);

    /**
     * 格式化文件的 URL 访问地址
     * 使用场景：local、ftp、db，通过 FileController 的 getFile 来获取文件内容
     *
     * @param domain 自定义域名
     * @param path 文件路径
     * @return URL 访问地址
     */
    default String formatFileUrl(String domain,Integer tenantCode, String path) {
        return String.format("%s/oss/download/%s/%s", domain, tenantCode, path);
    }

    /**
     * 根据link访问路径获取文件的绝对路径
     */
    default String getFilePath(String link,String domain,Integer tenantCode) {
        String pre = String.format("%s/oss/download/%s", domain,tenantCode);
        String path = link.replace(pre, StrUtil.EMPTY);
        return path;
    }
    /**
     * basePath 根目录
     * @param originalName 原始文件名
     * @return 文件全路径
     */
    default String getFilePath(String basePath, String originalName){
        String path = basePath+ "/"+ FileUtils.generatePath(originalName);
        return path;
    }


    /**
     * 插入文件内容
     * @param configId 配置编号
     * @param link 路径
     * @param content 内容
     */
    default void insert(Integer configId, String fileName, String link, byte[] content, OssTemplate.FileStorageEnum storage){
        JdbcTemplate jdbcTemplate = SpringContextConfig.getBean(JdbcTemplate.class);
        String sql = String.format("insert into %s (id,file_name,link,config_id,storage,content,tenant_code,create_by,create_time,create_dept) values(?,?,?,?,?,?,?,?,?,?)",OssTemplate.DB_OSS_TABLE_NAME);
        AuthInfo authInfo = AuthUtils.getAuthInfo();
        String tenantCode = authInfo==null?null:authInfo.getTenantCode();
        Integer createBy = authInfo==null?null:authInfo.getId();
        Integer createDept = authInfo==null?null:authInfo.getDeptId();
        jdbcTemplate.update(sql, IdWorker.getId(),fileName,link,configId,storage.ordinal(),content,tenantCode,createBy,new Date(),createDept);
    }

    /**
     * 插入文件内容
     * @param configId 配置编号
     * @param link 路径
     */
    default void insert(Integer configId, String fileName, String link, OssTemplate.FileStorageEnum storage){
        insert(configId, fileName,link,null, storage);
    }

    /**
     * 删除文件内容
     *
     * @param configId 配置编号
     * @param link 路径
     */
    default void delete(Integer configId, String link){
        JdbcTemplate jdbcTemplate = SpringContextConfig.getBean(JdbcTemplate.class);
        String sql = "delete from " + OssTemplate.DB_OSS_TABLE_NAME + " where config_id=" + configId + " and link='" + link+"'";
        jdbcTemplate.execute(sql);
    }
}
