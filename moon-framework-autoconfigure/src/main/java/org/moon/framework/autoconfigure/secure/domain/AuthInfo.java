/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.secure.domain;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户对象
 * @author ninglong
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthInfo {

	/** 用户ID */
    private Integer id;

    /** 账号 */
    private String userName;

    /** 昵称 */
    private String nickName;

    /** 手机号码 */
    private String phoneNumber;

    /** 租户code */
    private String tenantCode;

    /** 部门code */
    private Integer deptId;

    private List<String> roleCodes = Lists.newArrayList();

    private Boolean enabled;
    /**
     * 最后交互时间(优化刷新token,减少每次操作都要更新缓存)
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime optTime;

    /** token标识 */
    private String jti;

    /**token过期时间*/
    private Long exp;
}