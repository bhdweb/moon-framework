package org.moon.framework.autoconfigure.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**系统配制
 * @author moon
 */
@Configuration
@Data
public class SystemConfig {

    @Value("${spring.application.name}")
    private String applicationName;
    /**静态资源地址*/
	@Value("${moon.static-resource-url:http://172.16.1.50/}")
    private String staticResourceLocation;
    @Value("${moon.scp.tmpDir:D:/}")
    private String tmpDir;
    @Value("${moon.serverlog:/home/moon/logs/info.log}")
    private String serverlog;
}
