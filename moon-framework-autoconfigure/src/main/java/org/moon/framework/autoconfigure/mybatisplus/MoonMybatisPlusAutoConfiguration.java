/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.mybatisplus;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.StringValue;
import org.moon.framework.autoconfigure.MoonConstants;
import org.moon.framework.autoconfigure.mybatisplus.props.JdbcProperties;
import org.moon.framework.autoconfigure.secure.AuthUtils;
import org.moon.framework.autoconfigure.tenant.props.MoonTenantProperties;
import org.moon.framework.autoconfigure.utils.Func;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * mybatis plus 插件配置
 * @author moon
 */
@AutoConfigureAfter({com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration.class})
@Configuration
@EnableConfigurationProperties({JdbcProperties.class,MoonTenantProperties.class})
@Slf4j
public class MoonMybatisPlusAutoConfiguration {
	
	@Autowired
	private MoonTenantProperties moonTenantProperties;
    @Autowired
    private JdbcProperties jdbcProperties;

	/**
     * 新多租户插件配置,一缓和二缓遵循mybatis的规则,需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存万一出现问题
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {

        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        if(MoonConstants.TenantTypeEnum.column.name().equalsIgnoreCase(moonTenantProperties.getType())) {
            openTenantLine(interceptor);
        }
//        interceptor.addInnerInterceptor(new DataPermissionInterceptorHandler());
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        log.info("mybatis-plus 插件初始化完成.....");
        return interceptor;
    }

    /**
     * 开启cloumn级别的租户数据模式
     */
    private void openTenantLine(MybatisPlusInterceptor interceptor){
        log.info("系统已开启cloumn级别的租户数据模式....");
        interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(new TenantLineHandler() {
            @Override
            public Expression getTenantId() {
                return new StringValue(Func.toStr(AuthUtils.getTenantCode(), MoonConstants.DEFAULT_TENANT_CODE));
            }
            @Override
            public String getTenantIdColumn() {
                return moonTenantProperties.getColumn();
            }
            @Override
            public boolean ignoreTable(String tableName) {
                if(Func.isBlank(AuthUtils.getTenantCode())){
                    return true;
                }
                if(moonTenantProperties.getExcludeTables().contains(tableName)){
                    return true;
                }
                if(moonTenantProperties.getTables().size() > 0 && moonTenantProperties.getTables().contains(tableName)){
                    return true;
                }
                return false;
            }
        }));
    }
	@Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return configuration -> configuration.setUseDeprecatedExecutor(false);
    }
}