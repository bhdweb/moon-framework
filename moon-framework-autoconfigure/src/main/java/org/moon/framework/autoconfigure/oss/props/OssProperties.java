package org.moon.framework.autoconfigure.oss.props;

import lombok.Data;

/**
 * oss存储配置
 *
 * @author 村口的大爷
 */
@Data
public class OssProperties {

    /**
     * 配置ID
     */
    private Integer id;
    /**
     * 存储方式
     */
    private Integer storage;
    /**
     * 基础路径  存储 Bucket
     */
    private String basePath;

    /**
     * 自定义域名
     * 1. MinIO：通过 Nginx 配置
     * 2. 阿里云：https://help.aliyun.com/document_detail/31836.html
     * 3. 腾讯云：https://cloud.tencent.com/document/product/436/11142
     * 4. 七牛云：https://developer.qiniu.com/kodo/8556/set-the-custom-source-domain-name
     * 5. 华为云：https://support.huaweicloud.com/usermanual-obs/obs_03_0032.html
     */
    private String domain;
    /**
     * 主机地址
     */
    private String host;
    /**
     * 主机端口
     */
    private Integer port;

    /**
     * 用户名 或者 访问 Key
     * 1. MinIO：https://www.iocoder.cn/Spring-Boot/MinIO
     * 2. 阿里云：https://ram.console.aliyun.com/manage/ak
     * 3. 腾讯云：https://console.cloud.tencent.com/cam/capi
     * 4. 七牛云：https://portal.qiniu.com/user/key
     * 5. 华为云：https://support.huaweicloud.com/qs-obs/obs_qs_0005.html
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 连接模式
     *
     * 使用 {@link  cn.hutool.extra.ftp.FtpMode} 对应的字符串
     */
    private String mode;

    /**
     * 节点地址
     * 1. MinIO：https://www.iocoder.cn/Spring-Boot/MinIO 。例如说，http://127.0.0.1:9000
     * 2. 阿里云：https://help.aliyun.com/document_detail/31837.html
     * 3. 腾讯云：https://cloud.tencent.com/document/product/436/6224
     * 4. 七牛云：https://developer.qiniu.com/kodo/4088/s3-access-domainname
     * 5. 华为云：https://developer.huaweicloud.com/endpoint?OBS
     */
    private String endpoint;
}
