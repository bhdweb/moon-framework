/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.secure.props;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * security放行额外配置
 *
 * @author ninglong
 */
@Data
@ConfigurationProperties("moon.security")
public class SecureProperties {
	private Boolean tokenEnabled = false;
	private Boolean authEnabled = false;
	private Boolean formRepeatEnabled = true;
	private final List<String> excludePatterns = new ArrayList<>();
	private Integer expireTime;
	private Integer expireTimeOfPhone;
}