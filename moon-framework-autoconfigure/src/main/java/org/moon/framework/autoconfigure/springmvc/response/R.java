/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.springmvc.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.moon.framework.autoconfigure.MoonConstants;
import org.springframework.lang.Nullable;

import java.util.Optional;

/**
 * 返回结果
 * @author moon
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class R<T> {

	private String code;
	/** 返回的用户展示的详细信息*/
	private String msg;
	/** 返回的具体数据*/
	private T data;
	
	public static <T> R<T> status(Boolean status) {
		if(status) {
			return success();
		}
		return fail(MoonConstants.FAIL.getMsg());
	}

	public static <T> R<T> fail(String msg) {
		return fail(MoonConstants.FAIL.getCode(), msg);
	}
	
	public static <T> R<T> fail(String code, String msg) {
		R<T> r = new R<>();
		r.setCode(code);
		r.setMsg(msg);
		return r;
	}
	public static <T> R<T> fail(ErrorCode code) {
		return fail(code.getCode(),code.getMsg());
	}

	public static <T> R<T> success() {
		R<T> r = new R<>();
		r.setCode(MoonConstants.SUCCESS.getCode());
		r.setMsg(MoonConstants.SUCCESS.getMsg());
		return r;
	}

	public static <T> R<T> success(T t) {
		return success(t,MoonConstants.SUCCESS.getCode(),MoonConstants.SUCCESS.getMsg());
	}

	public static <T> R<T> success(T t,String code, String msg) {
		R<T> r = new R<>();
		r.setCode(code);
		r.setData(t);
		r.setMsg(msg);
		return r;
	}

	public static <T> R<T> success(T t,ErrorCode code) {
		R<T> r = new R<>();
		r.setCode(code.getCode());
		r.setData(t);
		r.setMsg(code.getMsg());
		return r;
	}

	/**
	 *  服务熔断标准返回
	 */
	public static <T> R<T> feign(T t){
		return success(t, MoonConstants.FEIGN);
	}

	/**
	 *  系统限流标准返回
	 */
	public static <T> R<T> limit(T t){
		return success(t,MoonConstants.LIMIT);
	}

	/**
	 * 是否正确返回
	 */
	public boolean isSuccess() {
		return MoonConstants.SUCCESS.getCode().equals(this.code);
	}
}