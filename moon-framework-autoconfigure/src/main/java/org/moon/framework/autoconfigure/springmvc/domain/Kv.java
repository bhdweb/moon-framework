/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.springmvc.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 键值对
 * @author moon
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Kv {

	private String label;
	private String value;
}
