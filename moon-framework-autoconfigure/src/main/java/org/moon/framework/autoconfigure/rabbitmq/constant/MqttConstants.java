package org.moon.framework.autoconfigure.rabbitmq.constant;

/**
 * mqtt常量
 */
public interface MqttConstants {

    /**
     * 消费者通道名
     */
    String CHANNEL_NAME_IN = "mqttInboundChannel";
    /**
     * 生产者通道名
     */
    String CHANNEL_NAME_OUT = "mqttOutboundChannel";

    String FACTORY_NAME = "mqttPahoClientFactory";
    String OPTIONS_NAME = "mqttConnectOptions";

}
