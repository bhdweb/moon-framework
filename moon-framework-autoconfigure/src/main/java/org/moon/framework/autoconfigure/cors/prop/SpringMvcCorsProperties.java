/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.cors.prop;

import com.google.common.collect.Lists;

import java.util.List;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * 跨域处理
 * @author moon
 */
@Data
@ConfigurationProperties(prefix = "moon.cors")
public class SpringMvcCorsProperties {

  private String path;

  private boolean enabled;

  private List<String> allowOrigins = Lists.newArrayList("*");
  private List<String> allowHeaders = Lists.newArrayList("*");
  private List<String> allowMethods = Lists.newArrayList("*");
  private List<String> allowExposeHeaders;
}
