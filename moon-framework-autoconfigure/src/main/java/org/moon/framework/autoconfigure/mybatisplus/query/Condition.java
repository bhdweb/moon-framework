/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.mybatisplus.query;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.moon.framework.autoconfigure.exception.domain.MoonException;
import org.moon.framework.autoconfigure.springmvc.domain.Query;
import org.moon.framework.autoconfigure.utils.Func;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 分页工具
 *
 * @author ninglong
 */
public class Condition {
	
	/**
	 * 获取page对象
	 * @param <T>
	 * @param entity
	 * @return
	 */
	public static <T> IPage<T> getPage(Query entity){
		IPage<T> page = new Page<T>(entity.getPageNo(),entity.getPageSize());
		return page;
	}

	/**
	 * 获取mybatis plus中的QueryWrapper
	 * @param entity 实体
	 * @param <T>    类型
	 * @return QueryWrapper
	 */
	public static <T> LambdaQueryWrapper<T> getQueryWrapper(T entity) {
		return getQueryWrapper(entity,entity);
	}

	/**
	 * 支持配置化的模糊查询
	 * @param entity 数据库返回的对象实体
	 * @param query  查询条件实体
	 * @param <T> 数据库返回的对象实体
	 * @return
	 */
	public static <T> LambdaQueryWrapper<T> getQueryWrapper(T entity, Object query) {
		QueryWrapper<T> qw = new QueryWrapper<T>();
		Class<?> queryClass = query.getClass();
		Field[] fields = queryClass.getDeclaredFields();
		for(Field field:fields){
			field.setAccessible(true);
			QueryField queryField = field.getAnnotation(QueryField.class);
			if(queryField==null)continue;
			Object value;
			try {
				value = field.get(query);
			} catch (Exception e) {
				throw new MoonException("获取属性性出错");
			}
			if(value==null)continue;
			if(Func.isBlank(value.toString()))continue;
			List<?> list = null;
			if(value instanceof List){
				list = (List<?>)value;
				if(list.size()==0)continue;
			}
			String condition = queryField.condition();
			String cloumn = Func.toUnderlineCase(field.getName());
			if(Func.isBlank(condition) || SqlCondition.EQUAL.equals(condition)) {
				qw.eq(cloumn, value);
			}else if(SqlCondition.LIKE.equals(condition)){
				qw.like(cloumn,value);
			}else if(SqlCondition.LIKE_LEFT.equals(condition)){
				qw.likeLeft(cloumn,value);
			}else if(SqlCondition.LIKE_RIGHT.equals(condition)){
				qw.likeRight(cloumn,value);
			}else if(SqlCondition.GE.equals(condition)){
				qw.ge(cloumn,value);
			}else if(SqlCondition.GT.equals(condition)){
				qw.gt(cloumn,value);
			}else if(SqlCondition.LE.equals(condition)){
				qw.le(cloumn,value);
			}else if(SqlCondition.LT.equals(condition)){
				qw.lt(cloumn,value);
			}else if(SqlCondition.NOT_IN.equals(condition)){
				String columnName = queryField.columnName();
				if(Func.isBlank(columnName))throw new MoonException("查询不包含条件时需要指定列名");
				qw.notIn(columnName,list);
			}else if(SqlCondition.IN.equals(condition)){
				String columnName = queryField.columnName();
				if(Func.isBlank(columnName))throw new MoonException("查询包含条件时需要指定列名");
				qw.in(columnName,list);
			}
		}
		return qw.lambda();
	}
}