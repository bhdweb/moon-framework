/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.cache;

import lombok.extern.slf4j.Slf4j;
import org.moon.framework.autoconfigure.MoonConstants;
import org.moon.framework.autoconfigure.cache.impl.CacheProviderImpl;
import org.moon.framework.autoconfigure.config.SpringContextConfig;
import org.moon.framework.autoconfigure.redis.RedisCacheAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * 缓存自动注入
 * @author moon
 */
@Slf4j
@AutoConfigureAfter(RedisCacheAutoConfiguration.class)
@Configuration
@ConditionalOnClass({ CacheProperties.Redis.class, RedisCacheConfiguration.class })
public class MoonCacheAutoConfiguration {

    @Bean
    public CacheService cacheService(){
        return new CacheProviderImpl();
    }

    /**
     * Redis消息监听器容器
     * 这个容器加载了RedisConnectionFactory和消息监听器
     * 可以添加多个监听不同话题的redis监听器，只需要把消息监听器和相应的消息订阅处理器绑定，该消息监听器
     * 通过反射技术调用消息订阅处理器的相关方法进行一些业务处理
     * @param connectionFactory 链接工厂
     * @return redis消息监听容器
     */
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        //可以添加多个 messageListener
        container.addMessageListener((message, pattern) -> {
            CacheService cacheService = SpringContextConfig.getBean(CacheService.class);
            RedisTemplate<String, Object> redisTemplate=  SpringContextConfig.getBean("redisTemplate");
            RedisSerializer<String> redisSerializer = redisTemplate.getStringSerializer();
            String msg= redisSerializer.deserialize(message.getBody());
            String topic = redisSerializer.deserialize(message.getChannel());
            log.info("redis topic:{},msg={}",topic,msg);
            cacheService.delLocalCache(msg);
        }, new PatternTopic(MoonConstants.REDIS_KEY_DEL_LOCAL));
        return container;
    }
}
