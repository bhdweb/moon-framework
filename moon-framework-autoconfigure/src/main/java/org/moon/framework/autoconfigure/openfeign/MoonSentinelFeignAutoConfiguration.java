package org.moon.framework.autoconfigure.openfeign;

import com.alibaba.cloud.sentinel.feign.SentinelFeignAutoConfiguration;
import feign.Logger;
import feign.RequestInterceptor;
import org.moon.framework.autoconfigure.openfeign.interceptor.FeignRequestInterceptor;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Sentinel Feign Config
 * @author ninglong
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass({ RequestInterceptor.class })
@AutoConfigureBefore(SentinelFeignAutoConfiguration.class)
public class MoonSentinelFeignAutoConfiguration {

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public FeignRequestInterceptor basicAuthRequestInterceptor() {
        return new FeignRequestInterceptor();
    }
}
