/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.springmvc.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 树形结构
 * @author moon
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MoonTree implements Serializable{

	private static final long serialVersionUID = 5650009829939734998L;
	
	private String id; 
	private String value;
	private String label;
	private String type;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<MoonTree> children;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Map<String,Object> meta;
}