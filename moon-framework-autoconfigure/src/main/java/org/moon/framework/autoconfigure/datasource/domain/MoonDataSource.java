/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.datasource.domain;

import lombok.Data;

/**
 * 数据源
 * @author moon
 */
@Data
public class MoonDataSource {

    /**
     * 数据源名称
     */
    private String name;
    /**
     * 驱动类
     */
    private String driverClass;
    /**
     * 数据库链接
     */
    private String jdbcUrl;
    /**
     * 数据库账号名
     */
    private String userName;
    /**
     * 数据库密码
     */
    private String password;
}
