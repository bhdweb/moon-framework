package org.moon.framework.autoconfigure.shutdown.properties;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * @author ninglong
 */
@ConfigurationProperties(prefix = "moon.tomcat.shutdown")
@Data
@RefreshScope
public class TomcatGracefulShutdownProperties {

  //单位秒
  private Integer waitTime = 30;


}
