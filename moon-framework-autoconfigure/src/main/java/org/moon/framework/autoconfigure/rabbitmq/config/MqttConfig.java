package org.moon.framework.autoconfigure.rabbitmq.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;

/**
 * MQTT消费者配置
 * <p>处理入站消息</p>
 */
@Configuration
@Data
@ConfigurationProperties(prefix="moon.mqtt")
@ConditionalOnProperty(value="moon.mqtt.enabled",havingValue = "true")
public class MqttConfig {

  /**
   * MQTT登录名
   */
  private String userName;

  /**
   * MQTT登录密码
   */
  private String password;

  /**
   * MQTT连接地址
   */
  private String urls;

  /**
   * 消费者clientId前缀（消费者）
   */
  private String clientId;

  /**
   * 消费者订阅主题（消费者）
   */
  private String topicNames;
  /**
   * MQTT会话心跳时间
   */
  private int keepAlive=60;
  /**
   * 消费者订阅主题设置服务质量（消费者）
   */
  private String consumerQos="1";

  /**
   * 消费者操作完成的超时时长（消费者）
   */
  private long completionTimeout=MqttPahoMessageDrivenChannelAdapter.DEFAULT_COMPLETION_TIMEOUT;
}
