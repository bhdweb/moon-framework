package org.moon.framework.autoconfigure.cache.impl;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.moon.framework.autoconfigure.MoonConstants;
import org.moon.framework.autoconfigure.cache.CacheService;
import org.moon.framework.autoconfigure.config.SpringContextConfig;
import org.moon.framework.autoconfigure.exception.domain.MoonException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * 两级缓存
 * 本地guava,远程redis
 * 1、缓存更新，先更新远程的，然后通过redis 订阅与发布模式更新本地
 * 2、获取数据，先取本地缓存，然后再取远程缓存
 */
public class CacheProviderImpl implements CacheService {

    private static int CACHE_MAXIMUM_SIZE = 500000;

    private static Map<String, Cache<String, Object>> _cacheMap = Maps.newConcurrentMap();

    static {

        Cache<String, Object> cacheContainer = CacheBuilder.newBuilder()
                .maximumSize(CACHE_MAXIMUM_SIZE)
                .expireAfterAccess(CacheService.DEFAULT_EXPIRETIME, TimeUnit.MILLISECONDS) //最后一次访问后的一段时间移出
                .recordStats()//开启统计功能
                .build();
        _cacheMap.put(String.valueOf(CacheService.DEFAULT_EXPIRETIME), cacheContainer);
    }

    private RedisTemplate<String, Object> getRedisTemplate(){

        RedisTemplate<String, Object> redisTemplate=  SpringContextConfig.getBean("redisTemplate");
        if(redisTemplate==null){
            throw new MoonException("请添加moon-framework-starter-redis依赖");
        }
        return redisTemplate;
    }

    /**
     * 查询缓存
     *
     * @param key 缓存键 不可为空
     **/
    @Override
    public String get(String key) {
        String obj = get(key, String.class);
        return obj;
    }

    /**
     * 查询缓存
     * @param key        缓存键 不可为空
     * @param expireTime 过期时间（单位：毫秒） 可为空
     **/
    @Override
    public String get(String key, Long expireTime) {
        return get(key,String.class,expireTime);
    }

    /**
     * 查询缓存
     * @param key        缓存键 不可为空
     * @param t          缓存对象类型
     **/
    @Override
    public <T> T get(String key, Class<T> t) {
        return get(key,t,-1L);
    }

    /**
     * 查询缓存
     * @param key        缓存键 不可为空
     * @param t          缓存对象类型
     * @param expireTime 过期时间（单位：毫秒） 可为空
     **/
    @Override
    public <T> T get(String key, Class<T> t, Long expireTime) {
        T obj = getOfLocal(key,  t, expireTime);
        if(obj==null){
            obj = getOfRedis(key, t, expireTime);
            //如果从远程缓存中获取到，则保存到本地缓存
            if(obj!=null){
                set(key, obj,expireTime);
            }
        }
        return obj;
    }

    /**
     * 从本地缓存获取
     */
    private  <T extends Object, M extends Object> T getOfLocal(String key, Class<T> t, Long expireTime) {
        Cache<String, Object> cacheContainer = getCacheContainer(expireTime);
        T obj = (T) cacheContainer.getIfPresent(key);
        return obj;
    }

    /**
     * 从远程 redis获取
     */
    private <T extends Object, M extends Object> T getOfRedis(String key, Class<T> t, Long expireTime) {
        ValueOperations<String, Object> operations = getRedisTemplate().opsForValue();
        T obj = (T) operations.get(key);
        return obj;
    }

    /**
     * 设置缓存键值  直接向缓存中插入值，这会直接覆盖掉给定键之前映射的值
     * @param key 缓存键 不可为空
     * @param obj 缓存值 不可为空
     **/
    @Override
    public <T> void set(String key, T obj) {
        set(key, obj, CacheService.DEFAULT_EXPIRETIME);
    }

    /**
     * 设置缓存键值  直接向缓存中插入值，这会直接覆盖掉给定键之前映射的值
     * @param key        缓存键 不可为空
     * @param obj        缓存值 不可为空
     * @param expireTime 过期时间（单位：毫秒） 可为空
     **/
    @Override
    public <T> void set(String key, T obj, Long expireTime) {
        if (obj == null) {
            return;
        }
        ValueOperations<String, Object> operations = getRedisTemplate().opsForValue();
        operations.set(key, obj);
        if(expireTime!=null && expireTime>0L){
            getRedisTemplate().expire(key, expireTime, TimeUnit.MILLISECONDS);
        }
        //通知更新本地缓存删除
        getRedisTemplate().convertAndSend(MoonConstants.REDIS_KEY_DEL_LOCAL,key);
    }

    /**
     * 移除缓存
     *
     * @param key 缓存键 不可为空
     **/
    @Override
    public void del(String key) {
        getRedisTemplate().delete(key);
        //通知更新本地缓存删除
        getRedisTemplate().convertAndSend(MoonConstants.REDIS_KEY_DEL_LOCAL,key);
    }

    /**
     * 根据key前缀删除
     */
    @Override
    public void cleanByPrefix(String keyPrefix) {
        Set<String> set = getRedisTemplate().keys(keyPrefix+"*");
        Iterator<String> it = set.iterator();
        while(it.hasNext()){
            String keyStr = it.next();
            del(keyStr);
        }
    }

    /**
     * 根据key前缀获取所有的key
     */
    @Override
    public List<String> getKeyByPrefix(String keyPrefix) {
        List<String> keys = Lists.newArrayList();
        Iterator<Map.Entry<String,Cache<String, Object>>> it = _cacheMap.entrySet().iterator();
        while (it.hasNext()) {
            Cache<String, Object> cache = it.next().getValue();
            keys.addAll(cache.asMap().keySet());
        }
        if(keys.isEmpty()){
            Set<String> set = getRedisTemplate().keys(keyPrefix+"*");
            keys = set.stream().collect(Collectors.toList());
        }
        return keys;
    }

    /**
     * 是否存在缓存
     * @param key 缓存键 不可为空
     **/
    @Override
    public boolean contains(String key) {
        if (StringUtils.isEmpty(key)) {
            return false;
        }
        boolean exists = false;
        ValueOperations<String, Object> operations = getRedisTemplate().opsForValue();
        Object obj = operations.get(key);
        if (obj != null) {
            exists = true;
        }
        return exists;
    }

    @Override
    public void delLocalCache(String key) {
        long expireTime = getExpireTime(CacheService.DEFAULT_EXPIRETIME);
        Cache<String, Object> cacheContainer = getCacheContainer(expireTime);
        cacheContainer.invalidate(key);
    }

    /**
     * 获取过期时间 单位：毫秒
     * @param expireTime 传人的过期时间 单位毫秒 如小于1分钟，默认为10分钟
     **/
    private Long getExpireTime(Long expireTime) {
        Long result = expireTime;
        if (expireTime == null || expireTime < CacheService.DEFAULT_EXPIRETIME / 10) {
            result = CacheService.DEFAULT_EXPIRETIME;
        }
        return result;
    }

    private static Lock lock = new ReentrantLock();

    private Cache<String, Object> getCacheContainer(Long expireTime) {

        if (expireTime == null) {
            return null;
        }
        String mapKey = String.valueOf(expireTime);
        Cache<String, Object> cacheContainer = null;
        if (_cacheMap.containsKey(mapKey) == true) {
            cacheContainer = _cacheMap.get(mapKey);
            return cacheContainer;
        }
        lock.lock();
        try {
            cacheContainer = CacheBuilder.newBuilder()
                    .maximumSize(CACHE_MAXIMUM_SIZE)
                    .expireAfterAccess(CacheService.DEFAULT_EXPIRETIME, TimeUnit.MILLISECONDS) //最后一次访问后的一段时间移出
                    .recordStats()//开启统计功能
                    .build();
            _cacheMap.put(mapKey, cacheContainer);
        } finally {
            lock.unlock();
        }
        return cacheContainer;
    }
}
