/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.cache;

import java.util.List;
import java.util.function.Function;

/**
 * 两级缓存
 * 本地guava,远程redis
 * 1、缓存更新，先更新远程的，然后通过redis 订阅与发布模式更新本地
 * 2、获取数据，先取本地缓存，然后再取远程缓存
 */
public interface CacheService {

    /**
     * 缓存默认过期时间1小时
     */
    long DEFAULT_EXPIRETIME = 1*60*60*1000L;

    /**
     * 获查询缓存
     * @param key 缓存键 不可为空
     * @return
     */
    String get(String key);

    /**
     * 获查询缓存
     * @param key 缓存键 不可为空
     * @param expireTime 过期时间（单位：毫秒） 可为空
     * @return
     */
    String get(String key,Long expireTime);

    /**
     * 查询缓存
     *
     * @param key 缓存键 不可为空
     **/
    <T extends Object> T get(String key,Class<T> t);

    /**
     * 查询缓存
     *
     * @param key        缓存键 不可为空
     *
     * @param expireTime 过期时间（单位：毫秒） 可为空
     **/
    <T extends Object> T get(String key, Class<T> t, Long expireTime);

    /**
     * 设置缓存键值
     *
     * @param key 缓存键 不可为空
     * @param obj 缓存值 不可为空
     **/
    <T extends Object> void set(String key, T obj);

    /**
     * 设置缓存键值
     *
     * @param key        缓存键 不可为空
     * @param obj        缓存值 不可为空
     * @param expireTime 过期时间（单位：毫秒） 可为空
     **/
    <T extends Object> void set(String key, T obj, Long expireTime);

    /**
     * 移除缓存
     *
     * @param key 缓存键 不可为空
     **/
    void del(String key);

    /**
     * 根据key前缀删除
     */
    void cleanByPrefix(String keyPrefix);

    /**
     * 根据key前缀获取所有的key
     */
    List<String> getKeyByPrefix(String keyPrefix);

    /**
     * 是否存在缓存
     *
     * @param key 缓存键 不可为空
     **/
    boolean contains(String key);

    /**
     * 移除本地缓存缓存
     * @param key 缓存键 不可为空
     **/
    void delLocalCache(String key);
}
