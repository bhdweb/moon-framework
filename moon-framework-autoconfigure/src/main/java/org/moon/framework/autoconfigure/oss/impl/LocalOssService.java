package org.moon.framework.autoconfigure.oss.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import org.moon.framework.autoconfigure.oss.OssService;
import org.moon.framework.autoconfigure.oss.OssTemplate;
import org.moon.framework.autoconfigure.oss.props.OssProperties;

import java.util.List;

/**
 * 本地文件客户端
 *
 * @author 芋道源码
 */
public class LocalOssService implements OssService {

    private OssProperties ossProperties;

    public LocalOssService(OssProperties ossProperties) {
        this.ossProperties = ossProperties;
    }

    @Override
    public String upload(byte[] content, String originalName) {
        // 执行写入
        String path = getFilePath(ossProperties.getBasePath(),originalName);
        FileUtil.writeBytes(content, path);
        // 拼接返回路径
        String link =  formatFileUrl(ossProperties.getDomain(),ossProperties.getId(), path);
        insert(ossProperties.getId(),originalName,link, OssTemplate.FileStorageEnum.LOCAL);
        return link;
    }

    @Override
    public void delete(String link) {
        String filePath = getFilePath(link,ossProperties.getDomain(),ossProperties.getId());
        FileUtil.del(filePath);
        delete(ossProperties.getId(),link);
    }

    @Override
    public void batchDelete(List<String> links) {
        for(String link:links){
            delete(link);
        }
    }

    @Override
    public byte[] getContent(String link) {
        String filePath = getFilePath(link,ossProperties.getDomain(),ossProperties.getId());
        return FileUtil.readBytes(filePath);
    }
}