/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.mybatisplus.props;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.function.Supplier;

/**
 * 数据库连接属性
 * @author moon
 */
@Data
@ConfigurationProperties(prefix = "spring.datasource")
public class JdbcProperties {

  public static final Supplier<JdbcPoolProperties> JDBC_POOL = JdbcPoolProperties::new;
  private String username;
  private String password;
  private String url;
  private String driverClassName = "com.mysql.cj.jdbc.Driver";
  private JdbcPoolProperties jdbcPool = JDBC_POOL.get();
}
