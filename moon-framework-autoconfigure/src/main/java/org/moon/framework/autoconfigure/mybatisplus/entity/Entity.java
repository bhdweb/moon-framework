package org.moon.framework.autoconfigure.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 不带id的基础实体类
 * @author 村口的大爷
 */
@Getter
@Setter
public class Entity implements Serializable {

    @ApiModelProperty(value = "状态（0正常 1删除）",allowableValues = "0,1",hidden = true)
    @TableLogic
    @JsonIgnore
    private Integer deleted;

    @ApiModelProperty(value = "创建者",hidden = true)
    @TableField(fill = FieldFill.INSERT)
    @JsonIgnore
    private Integer createBy;

    @ApiModelProperty(value = "创建者所在部门",hidden = true)
    @TableField(fill = FieldFill.INSERT)
    @JsonIgnore
    private Integer createDept;

    @ApiModelProperty(value = "创建时间",hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT + 8")
    @TableField(fill = FieldFill.INSERT)
    @JsonIgnore
    private Date createTime;

    @ApiModelProperty(value = "更新者",hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonIgnore
    private Integer updateBy;

    @ApiModelProperty(value = "更新时间",hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT + 8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonIgnore
    private Date updateTime;
}
