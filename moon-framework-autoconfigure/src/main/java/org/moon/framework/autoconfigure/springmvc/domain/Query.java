/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.springmvc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 分页查询参数
 * @author moon
 */
@Getter
@Setter
public class Query {

	@ApiModelProperty(value = "当前页")
	private int pageNo=1;
	@ApiModelProperty(value = "从哪一条数据开始",hidden = false)
	@JsonIgnore
	private int start;
	@ApiModelProperty(value = "每页显示")
	private int pageSize=10;
	
	public int getStart(){
		int start = 0;
    	if(pageNo>1){
    		start = (pageNo-1) * pageSize;
    	}
    	return start;
	}
}
