package org.moon.framework.autoconfigure.mybatisplus.query;

/**
 * SQL 比较条件常量定义类
 * @author ninglong
 * @create 2020-08-25 20:10
 */
public class SqlCondition extends com.baomidou.mybatisplus.annotation.SqlCondition {

    /**
     * IN
     */
    public static final String IN = "%s in(${%s})";
    /**
     * NOT IN
     */
    public static final String NOT_IN = "%s not in(${%s})";

    /**
     * 大于
     */
    public static final String LT = "%s > %s";

    /**
     * 大于等于
     */
    public static final String LE = "%s >= %s";

    /**
     * 小于
     */
    public static final String GT = "%s < %s";

    /**
     * 小于等于
     */
    public static final String GE = "%s <= %s";
}
