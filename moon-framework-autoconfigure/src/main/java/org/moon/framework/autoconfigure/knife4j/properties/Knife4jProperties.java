/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.knife4j.properties;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.List;

/**
 * knife4j配制属性
 * 
 * @author ninglong
 */
@Data
@ConfigurationProperties(prefix = "moon.knife4j")
@RefreshScope
public class Knife4jProperties {

	private String title = "广寒宫服务端框架";
	private String description = "广寒宫基于springboot2构建的服务端框架";
	private String version = "v1";
	private String termsOfServiceUrl = "www.guanghangong.xyz";
	private String contactName = "宁龙";
	private String contactUrl = "";
	private String contactEmail = "ninglong@moon.com";
	private String license = "世界那么大，钱包那么扁，可我还是想去看看";
	private String licenseUrl = "www.guanghangong.xyz";

	private String basePackage;
	private String groupName;

	private List<String> excludePath;
	private String accessToken;
}
