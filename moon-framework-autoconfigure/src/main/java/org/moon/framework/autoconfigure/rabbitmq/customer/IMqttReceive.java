package org.moon.framework.autoconfigure.rabbitmq.customer;

/**
 * 消息接收
 * @author moon
 */
public interface IMqttReceive {

    void handlerMessage(String message);
}
