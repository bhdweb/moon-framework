package org.moon.framework.autoconfigure.sms;

import org.moon.framework.autoconfigure.sms.domain.SmsResponse;

import java.util.List;
import java.util.Map;

/**
 * 短信发送
 */
public interface SmsService {

    /**
     * 发送单条短信
     * @param mobile  手机号码
     * @param templateCode  短信模板
     * @param templateParams 参数
     */
    SmsResponse sendSingleSms(String mobile, String templateCode, Map<String, String> templateParams);

    /**
     * 发送单条短信
     * @param mobiles  多个手机号码
     * @param templateCode  短信模板
     * @param templateParams 参数
     */
    SmsResponse sendMultiSms(List<String> mobiles, String templateCode, Map<String, String> templateParams);
}
