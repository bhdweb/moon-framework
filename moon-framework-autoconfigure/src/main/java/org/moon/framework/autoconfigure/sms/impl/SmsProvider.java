package org.moon.framework.autoconfigure.sms.impl;

import org.moon.framework.autoconfigure.sms.SmsService;
import org.moon.framework.autoconfigure.sms.props.SmsProperties;

/**
 * 框架内部使用不对外
 * @author moon
 */
public interface SmsProvider {

    /**
     * 构建短信发送对象
     */
    SmsService builder(SmsProperties smsProperties);
}
