package org.moon.framework.autoconfigure.log.domain;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 操作日志记录
 * 
 * @author ninglong
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperateLogger {

    /**
     * 链路追踪编号
     */
    private String requestId;
    /**
     * 请求来源
     */
    private String requestFrom;

    /**
     * 应用名
     * 目前读取 spring.application.name
     */
    private String applicationName;

    /**
     * 操作模块
     */
    private String module;
    /**
     * 操作人昵称
     */
    private String nickName;

    /**
     * 操作名
     */
    private String name;

    /**
     * 操作明细
     */
    private String content;

    /**
     * 拓展字段
     */
    private Map<String, Object> exts;

    /**
     * 请求方法名
     */
    private String requestMethod;

    /**
     * 请求地址
     */
    private String requestUrl;

    /**
     * 用户 IP
     */
    private String userIp;

    /**
     * 浏览器 UserAgent
     */
    private String userAgent;

    /**
     * Java 方法名
     */
    private String javaMethod;

    /**
     * Java 方法的参数
     */
    private String javaMethodArgs;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 执行时长，单位：毫秒
     */
    private Integer duration;

    /**
     * 结果码
     */
    private String resultCode;

    /**
     * 结果提示
     */
    private String resultMsg;

    /**
     * 结果数据
     */
    private String resultData;

    /**
     * 租户
     */
    private String tenantCode;

    /**
     * 创建者
     */
    private Integer createBy;

    /**
     * 创建者所在部门
     */
    private Integer createDept;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private Integer updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;
}
