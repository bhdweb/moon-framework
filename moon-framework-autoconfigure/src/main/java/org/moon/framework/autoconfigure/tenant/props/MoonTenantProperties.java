/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.moon.framework.autoconfigure.tenant.props;

import lombok.Getter;
import lombok.Setter;
import org.moon.framework.autoconfigure.MoonConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 多租户配置
 * @author moon
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "moon.tenant")
public class MoonTenantProperties {

	/**
	 * 是否开启多租户数据源全局扫描
	 */
	private Boolean datasourceGlobal = Boolean.FALSE;

	/**
	 * 租户类型
	 */
	private String type= MoonConstants.TenantTypeEnum.none.name();

	/**
	 * 多租户字段名称
	 */
	private String column = "tenant_code";

	/**
	 * 多租户数据表
	 */
	private List<String> tables = new ArrayList<>();

	/**
	 * 需要排除进行自定义的多租户表
	 * sys_role_menu  由于10000租户可以创建其它租户并授权，所以这个表需要排除
	 */
	private List<String> excludeTables = Arrays.asList("tables","columns","sys_role_menu","sys_dict","sys_tenant","sys_menu","sys_id","sys_config","sys_region","gen_table","gen_table_column");
}
