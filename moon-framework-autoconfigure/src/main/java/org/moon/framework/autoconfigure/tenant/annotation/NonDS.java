package org.moon.framework.autoconfigure.tenant.annotation;

import java.lang.annotation.*;

/**
 * 排除租户数据源自动切换.
 * @author moon
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NonDS {

}
