package org.moon.framework.autoconfigure.sms.props;

import lombok.Data;

/**
 * 短信配置项
 */
@Data
public class SmsProperties {
    /**
     * 是否启用
     */
    private Boolean enabled;

    /**
     * 短信服务名称
     */
    private String name;

    /**
     * regionId
     */
    private String regionId = "cn-hangzhou";

    /**
     * accessKey
     */
    private String accessKey;

    /**
     * secretKey
     */
    private String secretKey;

    /**
     * 短信签名
     */
    private String signName;

    /**
     * 域名
     */
    private String domain;
    /**
     * 本机IP
     */
    private String localIp;
}
