/**
 * 广寒宫
 * 网址:www.guanghangong.xyz
 */
package org.moon.framework.autoconfigure.tenant.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.moon.framework.autoconfigure.mybatisplus.entity.BaseEntity;

/**
 * 带租户编号的基础实体类
 * @author moon
 */
@Getter
@Setter
public class TenantEntity extends BaseEntity {

	private static final long serialVersionUID = 6659028015010451164L;

	@ApiModelProperty(value = "租户编号",hidden = true)
	@JsonIgnore
	private String tenantCode;
}
