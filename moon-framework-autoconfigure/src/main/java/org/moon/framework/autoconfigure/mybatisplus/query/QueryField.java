package org.moon.framework.autoconfigure.mybatisplus.query;

import java.lang.annotation.*;

/**
 * 实体查询比较条件
 * @author ninglong
 * @create 2020-08-25 20:14
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface QueryField {

    /**
     * 字段 where 实体查询比较条件
     * 默认 `=` 等值
     */
    String condition() default SqlCondition.EQUAL;


    /**
     * 数据库表的列名, 默认实体属性
     * @return
     */
    String columnName() default "";

    /**
     * In或者NotIn的列名
     * @return
     */
    String inColumnName() default "";
}
