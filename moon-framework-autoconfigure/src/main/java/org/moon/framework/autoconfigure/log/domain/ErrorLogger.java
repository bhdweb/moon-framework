package org.moon.framework.autoconfigure.log.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 错误日志
 *
 * @author moon
 */
@Data
@Accessors(chain = true)
public class ErrorLogger {

    /**
     * 链路编号
     */
    private String requestId;

    /**
     * 请求来源
     */
    private String requestFrom;
    /**
     * 应用名
     */
    private String applicationName;

    /**
     * 请求方法名
     */
    private String requestMethod;
    /**
     * 访问地址
     */
    private String requestUrl;
    /**
     * 请求参数
     */
    private String requestParams;
    /**
     * 用户 IP
     */
    private String userIp;
    /**
     * 浏览器 UA
     */
    private String userAgent;

    /**
     * 异常时间
     */
    private Date exceptionTime;
    /**
     * 异常名
     */
    private String exceptionName;
    /**
     * 异常发生的类全名
     */
    private String exceptionClassName;
    /**
     * 异常发生的类文件
     */
    private String exceptionFileName;
    /**
     * 异常发生的方法名
     */
    private String exceptionMethodName;
    /**
     * 异常发生的方法所在行
     */
    private Integer exceptionLineNumber;
    /**
     * 异常的栈轨迹异常的栈轨迹
     */
    private String exceptionStackTrace;
    /**
     * 异常导致的根消息
     */
    private String exceptionRootCauseMessage;
    /**
     * 异常导致的消息
     */
    private String exceptionMessage;

    /**
     * 操作人昵称
     */
    private String nickName;

    /**
     * 租户
     */
    private String tenantCode;

    /**
     * 创建者
     */
    private Integer createBy;

    /**
     * 创建者所在部门
     */
    private Integer createDept;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private Integer updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;
}
